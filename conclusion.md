### git
(1) 基本概念
版本管理工具
版本库 .git文件 ---当我们使用git管理文件时，一般它会多出一个git文件，我们称之为版本库

git区划分:本地工作区,暂存区,历史版本区(本地仓库),远程仓库

(2) git推送操作 
1.git clone <分支地址>    将分支克隆到本地中
2.git remotev -v     查看是否链接到远程
3.git checkout -b <分支名>  新建一个本地分支并切换到该分支上
4.git add ./-A   将文件发送到暂存区
5.git commit -m "操作描述"    将暂存区的文件发送到历史版本区
6.git push   将文件推送到远程仓库上

(3) git常用指令,分支管理
1.git branch  查看本地分支
2.git branch -r  查看远程分支
3.git branch -a  查看所有分支
4.git checkout -b <分支名>  创建本地分支并切换到该分支
5.git checkout <分支名>  从当前分支切换到指定分支
6.git branch -d <分支名>  删除本地分支
7.git merge <分支名>  合并指定分支
8.git log  查看历史提交记录
9.git stash  暂存文件
10.git stash pop  取出暂存文件
11.git status 查看工作树是否干净

### 项目基础建设
1.封装 git hooks
-下载依赖包:npm i hooks
-执行指令hooks install   生成.hooks文件夹
-添加hook钩子(husky哈士奇) `npm husky add .husky/commit-msg 'npx --no-install commitlint --edit'`
                          `commit-msg git commit -m "`

commitlint语法

@commitlint/cli  cli指令

创建commit.config.js配置文件  配置校验规则 

配置校验:module.exports={
    `extends ['@commitlint/config-conventional']`
}

下载@commitlint/config-conventional   cli校验规则指令

2.eslint

下载lint-staged
在scripts配置文件中添加lint字段 运行lint-staged指令
监听文件的变化
添加 `git hooks pre-commit`,`git add .`,`npx husky add .husky/pre-commit "npm run lint"`
husky生成钩子 npx husky add .husky/钩子的名称  执行命令


### axios 二次封装
1.方便统一管理接口地址
2.接口多次复用
3.ajax的作用的发送http请求,是window对象提供XMLHttpRequests对象
4.axios是浏览器和服务器的通信工具
5.接口请求的then的响应值取决于拦截器的返回值